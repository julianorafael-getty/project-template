# PROJECT TEMPLATE #

This our project template repo. Every project should fork this project to get its branching structure and policies.

# CONTRIBUTING GUIDE #

1. Communicate context and requirements with reviewers
	* Identify the best person/people to review your change
	* Communicate your change and what it’s purpose to your reviewers
	* Establish your timeline with all reviewers if you need to ship by a specific date
	* Identify a primary reviewer
	* Define each reviewer’s responsibilities
1. Carefully read your code before publishing
	* Read your diff
	* Make sure your diff clearly represents your changes.
	* Can your code review be broken into smaller chunks?
	* Make sure your code is easy for reviewers to follow
	* Style check your code
	* Make any relevant documentation easily available for reviewers
	* Confirm that your reviewers are aware of any major changes (if any) you plan on making during review
1. Discuss feedback
	* Respond to all code review feedback. Discuss any feedback you disagree with
	
# REVIEWING GUIDE #

1. Understand the code
	* Make sure you completely understand the code
	* Evaluate all the architecture tradeoffs
1. Check code quality
	* Verify code correctness
	* Check for well-organized and efficient core logic
	* Is the code as general as it needs to be, without being more general that it has to be?
	* Make sure the code is maintainable
	* Enforce stylistic consistency with the rest of the codebase
1. Verify that the code is tested well
	* Confirm adequate test coverage
	* Check tests having the right dependencies and are testing the right things
1. Make sure the code is safe to deploy
	* Ask if the code is forwards/backwards compatible. In particular, be on the lookout if the code is changing the serialization / deserialization of something
	* Run through a roll-back scenario to check for rollback safety
1. Check for any security holes and loop in the security team if you’re unsure
1. Answer: If this code breaks at 3am and you’re called to diagnose and fix this issue, will you be able to?

